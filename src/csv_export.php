<?php

$name = $_POST["name"];
$email = $_POST["email"];

if (isset($name) && isset($email)) {
  $name = filter_var($name, FILTER_SANITIZE_STRING);
  $email = filter_var($email, FILTER_SANITIZE_EMAIL);

  $dataSting = $name . "," . $email . ",";

  // if (!file_exists("data.csv")) {
  //   touch("data.csv");
  // }

  $filePointer = fopen("data.csv", "a"); // "a" means Open file only for writing and appending
  fwrite($filePointer, $bom = (chr(0xEF) . chr(0xBB) . chr(0xBF))); // Add Byte Order Mark to fix UTF-8 in Excel

  if($filePointer) {
    fwrite($filePointer, $dataSting . "\n"); // Append data to the file and insert new line
    fclose($filePointer);
  }
}

?>
